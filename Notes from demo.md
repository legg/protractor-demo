Protractor
----------
Install nodejs and protractor notes

npm install -D protractor

Webdriver-manager update

Download and install the JDK or JRE
http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html

webdriver-manager start (should start without errors) (then goto http://127.0.0.1:4444/wb/hub)

you're now mostly setup. Install WebStorm if you want, or Sublime, but not required. You just need a terminal and text editor.

Next:
---------------
install all dependencies
    npm install

run the end to end tests using protractor
    npm run protractor

run unit tests using karma
    npm test

Topics covered from demo:
-------------------------
    NPM
    Angular Project
    WebStorm
    Karma
    Jasmine
    Cucumber
    Protractor
    - locators
    -- by id
    -- by model
    -- by repeater
    Promise
    Prototypical OOP
