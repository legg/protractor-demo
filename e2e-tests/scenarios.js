'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {

  browser.get('index.html');

  it('should automatically redirect to /view1 when location hash/fragment is empty', function() {
    expect(browser.getLocationAbsUrl()).toMatch("/view1");
    expect(protractor.promise).toBeDefined();
    expect(protractor.promise).toBe(Q);
  });


  describe('view1', function() {

    beforeEach(function() {
      browser.get('index.html#/view1');
    });


    it('should render view1 when user navigates to /view1', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 1/);
    });

    it('should show three items in the list', function(){
      var list = element.all(by.repeater('item in list'));
      expect(list).toBeDefined();
      expect(list.get(0).getText()).toBe('first');
      expect(list.get(1).getText()).toBe('second');
      expect(list.get(2).getText()).toBe('third');
    });

    it('should the page description', function(){
      expect(element(by.binding('pageDescription'))).toBeDefined();
    });

  });


  describe('view2', function() {

    beforeEach(function() {
      browser.get('index.html#/view2');
    });


    it('should render view2 when user navigates to /view2', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 2/);
    });

    //it('doe3nst do anything ', function(){
    //  var box = element(by.id('asdfdsa'));
    //
    //  box.then(function(theBox){
    //    expect(theBox).toBeDefined();
    //  }, function(failure){
    //    // maybe retry;
    //  });
    //})

  });
});
