'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', function($scope) {
  $scope.list = [
    {id:1,text:'first'},
    {id:2,text:'second'},
    {id:3,text:'third'}
  ];
      $scope.pageDescription = 'this is a page description on the model of the scope';
}]);